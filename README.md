# Mining Covid-19 Patient Data (Final Milestone)

## Objective
- This project aims to build a robust classification model to accurately classify the health status of COVID-19 patients (hospitalized, non-hospitalized, recovered, deceased) using the provided COVID dataset.

## EDA & Data Cleaning
-  Column information were used to systematically exclude features with one of the following characteristics:
	- All the values in a column are the same
	- All the values in a column are unique to its corresponding row e.g., id number
	- Duplicate columns either in values or in representation e.g., location coordinate & location name

- Imputations were done to fill-in or replace values in relevant features
	- Age: empty values were filled with the mean of the entire column
	- Sex: empty values were filled with "Unknown"
	- Date Confirmation: empty values were filled with the latest date
	- Missing values in text columns: filled with '0' 
	- Non-missing values in text columns: replaced with '1'.
	- Location: applied GeoPy package to Longitude & Latitude  coordinates to reverse map locations

## Outlier detection via Interquartile Range
- No outliers for nominal values
- Negative values were kept for subsequent group aggregation
- Regions that do not translate to proper locations are assigned with "Unknown" as a location value

## Data Transformation & Merging
- Demographic data points were aggregated into a single record and subsequently merged with patient data.

## Classification Models
- K-Nearest Neighbors
- Random Forests
- LightGBM

## Model Evaluation & Label Imbalance
- Built a 5-fold cross-validation pipeline to select the most optimal classifier via hyperparameter tuning.
- Class label imbalance was handled by applying SMOTE to the most under-represented group

## Results: Predictions & Accuracy Assessment Metrics
- Random Forest and LightGBM are comparable with respect to f1-score, recall and precision.
- KNN performed the worst.
- All 3 classifiers have trouble classifying data points belong to the under-represented class.

## Results
- To improve classification accuracy, a thorough data-centric EDA approach is required.
